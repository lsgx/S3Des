# S3Des

加密或者解密3DES文件的小工具

![help_and_credits](screenshot/help_and_credits.png)
![decrypt_and_output](screenshot/decrypt_and_output.png)
![encrypt_and_output](screenshot/encrypt_and_output.png)


## 环境要求

若要运行应用程序，您必须首先安装 .NET Framework 4.0

## 特别声明

程序仅供测试使用，作者不负任何责任。
